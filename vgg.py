import torch.nn as nn
import torch.nn.functional as F
import torch

import torchvision.models as models

from collections import namedtuple


class VGG(nn.Module):
    def __init__(self, pool="max"):
        super().__init__()

        # Convolution layers
        self.conv1_1 = nn.Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv1_2 = nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv2_1 = nn.Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv2_2 = nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv3_1 = nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv3_2 = nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv3_3 = nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv4_1 = nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv4_2 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv4_3 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv5_1 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv5_2 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))
        self.conv5_3 = nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1))

        # Average Pooling layers
        if pool == "max":
            self.p1 = nn.MaxPool2d(kernel_size=2, stride=2)
            self.p2 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p3 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p4 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p5 = nn.AvgPool2d(kernel_size=2, stride=2)
        else:
            if pool != "avg":
                print("Invalid pool parameter type. Setting average pool layers")
            self.p1 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p2 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p3 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p4 = nn.AvgPool2d(kernel_size=2, stride=2)
            self.p5 = nn.AvgPool2d(kernel_size=2, stride=2)

    def forward(self, x, out_params=None):
        out = {}

        out["relu1_1"] = F.relu(self.conv1_1(x))
        out["relu1_2"] = F.relu(self.conv1_2(out["relu1_1"]))
        out["p1"] = self.p1(out["relu1_2"])

        out["relu2_1"] = F.relu(self.conv2_1(out["p1"]))
        out["relu2_2"] = F.relu(self.conv2_2(out["relu2_1"]))
        out["p2"] = self.p2(out["relu2_2"])

        out["relu3_1"] = F.relu(self.conv3_1(out["p2"]))
        out["relu3_2"] = F.relu(self.conv3_2(out["relu3_1"]))
        out["relu3_3"] = F.relu(self.conv3_3(out["relu3_2"]))
        out["p3"] = self.p3(out["relu3_3"])

        out["relu4_1"] = F.relu(self.conv4_1(out["p3"]))
        out["relu4_2"] = F.relu(self.conv4_2(out["relu4_1"]))
        out["relu4_3"] = F.relu(self.conv4_3(out["relu4_2"]))
        out["p4"] = self.p4(out["relu4_3"])

        out["relu5_1"] = F.relu(self.conv5_1(out["p4"]))
        out["relu5_2"] = F.relu(self.conv5_2(out["relu5_1"]))
        out["relu5_3"] = F.relu(self.conv5_3(out["relu5_2"]))
        out["p5"] = self.p5(out["relu5_3"])

        if out_params is not None:
            return [out[param] for param in out_params]

        vgg_outputs = namedtuple("VggOutputs", ["relu1_2", "relu2_2", "relu3_3", "relu4_3"])
        out = vgg_outputs(out["relu1_2"], out["relu2_2"], out["relu3_3"]. out["relu4_3"])

        return out

    def load_weights(self, pretrained_model=None):
        if pretrained_model is None:
            print("Downloading pretrained model...")
            pretrained_model = models.vgg16(pretrained=True)

        pretrained_model_features = pretrained_model.features

        with torch.no_grad():
            self.conv1_1.weight.copy_(pretrained_model_features[0].weight)
            self.conv1_2.weight.copy_(pretrained_model_features[2].weight)

            self.conv2_1.weight.copy_(pretrained_model_features[5].weight)
            self.conv2_2.weight.copy_(pretrained_model_features[7].weight)

            self.conv3_1.weight.copy_(pretrained_model_features[10].weight)
            self.conv3_2.weight.copy_(pretrained_model_features[12].weight)
            self.conv3_3.weight.copy_(pretrained_model_features[14].weight)

            self.conv4_1.weight.copy_(pretrained_model_features[17].weight)
            self.conv4_2.weight.copy_(pretrained_model_features[19].weight)
            self.conv4_3.weight.copy_(pretrained_model_features[21].weight)

            self.conv5_1.weight.copy_(pretrained_model_features[24].weight)
            self.conv5_2.weight.copy_(pretrained_model_features[26].weight)
            self.conv5_3.weight.copy_(pretrained_model_features[28].weight)
