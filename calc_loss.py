import torch.nn as nn
import torch


class GramMatrix(nn.Module):
    def forward(self, input):
        b, c, h, w = input.size()

        f = input.view(b, c, h * w) # b * c * (hxv)

        G = torch.bmm(f, f.transpose(1, 2))

        return G.div_(h * w)


class StyleLoss(nn.Module):
    def forward(self, input, target):
        GramInput = GramMatrix()(input)
        return nn.MSELoss()(GramInput, target)