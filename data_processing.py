from torchvision import transforms as tf
import torch
import torchvision.utils as tutils
from PIL import Image
from torch.autograd import Variable

transform = tf.Compose([
    tf.Resize(512), # default image size
    tf.ToTensor(), # Transform it to a torch tensor
    tf.Lambda(lambda x: x[torch.LongTensor([2, 1, 0])]), # converting RGB to BGR
    tf.Normalize(mean=[0.40760392, 0.45795686, 0.48501961], std=[0.225, 0.224, 0.229]),  # subracting imagenet mean
    tf.Lambda(lambda x: x.mul_(255)),
])

def load_image(path):
    img = Image.open(path)
    img = Variable(transform(img))
    img = img.unsqueeze(0)
    return img

def save_image(img, name):
    post = tf.Compose([
        tf.Lambda(lambda x: x.mul_(1. / 255)),
        tf.Normalize(mean=[-0.40760392, -0.45795686, -0.48501961], std=[1, 1, 1]),
        tf.Lambda(lambda x: x[torch.LongTensor([2, 1, 0])]),  # turn to RGB
    ])
    img = post(img)
    img = img.clamp_(0, 1)
    tutils.save_image(img,
                      f"./images/{name}",
                      normalize=True)

