import torchvision.models as models
from vgg import VGG
import torch

vgg_pretrained_features = models.vgg19(pretrained=True).features
mv = VGG()

print(vgg_pretrained_features[0].weight)
print(mv.conv1_1)

# print(vgg_pretrained_features[0].weight)
with torch.no_grad():
    mv.conv1_1.weight.copy_(vgg_pretrained_features[0].weight)
