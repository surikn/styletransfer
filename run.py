from data_processing import load_image, save_image
from vgg import VGG
from calc_loss import GramMatrix, StyleLoss
import torchvision.models as models

import torch.nn as nn
import torch.backends.cudnn as cudn
import torch.optim as optim
from torch.autograd import Variable
import torch
import random
import sys
import os

# defining content and style layers
content_layers = ["relu4_2"]
style_layers = ["relu1_1", "relu2_1", "relu3_1", "relu4_1", "relu5_1"]

# setting pytorch seed
torch.cuda.manual_seed_all(random.randint(1, 10000))

if not os.path.exists("./images"):
    os.makedirs("./images")

# enable the cudn auto-tuner to find the best algo for this hardware
cudn.benchmark = True


def print_usage():
    print("Usage: kek")


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print_usage()
        sys.exit(1)
    style_img_path = sys.argv[1]
    content_img_path = sys.argv[2]

    style_image = load_image(style_img_path).cuda()
    content_image = load_image(content_img_path).cuda()

    vgg_directory = "./weights.pth" # pretrained vgg directory
    pretrained_vgg = models.vgg16()
    pretrained_vgg.load_state_dict(torch.load(vgg_directory))

    vgg = VGG()  # TODO: make arguments tunable (pool type)
    vgg.load_weights(pretrained_model=pretrained_vgg)

    # freeze model
    for param in vgg.parameters():
        param.requires_grad = False

    vgg.cuda()

    style_targets = []
    for t in vgg.forward(style_image, out_params=style_layers):
        t = t.detach()
        style_targets.append(GramMatrix()(t))

    content_targets = []
    for t in vgg.forward(content_image, out_params=content_layers):
        t = t.detach()
        content_targets.append(t)

    style_losses = [StyleLoss()] * len(style_layers)
    content_losses = [nn.MSELoss()] * len(content_layers)

    losses = style_losses + content_losses
    targets = style_targets + content_targets
    loss_layers = style_layers + content_layers
    style_weight = 1000  # TODO: make this parameter tunable
    content_weight = 7  # TODO: make this parameter tunable
    weights = [style_weight] * len(style_layers) + [content_weight] * len(content_layers)

    optim_image = Variable(content_image.data.clone(), requires_grad=True)
    optimizer = optim.LBFGS([optim_image])

    # Shifting everything to cuda
    for loss in losses:
        loss = loss.cuda()
    optim_image.cuda()

    iterations = 100  # TODO: make this parameter tunable
    save_every = 5

    for it in range(1, iterations + 1):
        print(f"Iteration {it}/{iterations}")

        def cl():
            optimizer.zero_grad()
            out = vgg.forward(optim_image, loss_layers)
            total_loss_list = []

            for i in range(len(out)):
                layer_output = out[i]
                loss_i = losses[i]
                target_i = targets[i]
                total_loss_list.append(loss_i(layer_output, target_i) * weights[i])

            total_loss = sum(total_loss_list)
            total_loss.backward()
            print(f"Loss: {total_loss}")
            return total_loss

        optimizer.step(cl)
        if it % save_every == 0:
            outImg = optim_image.data[0].cpu()
            save_image(outImg.squeeze(), f"proc{it}.png")

    outImg = optim_image.data[0].cpu()
    save_image(outImg.squeeze(), "result.png")
